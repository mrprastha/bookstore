<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
    use HasFactory,Notifiable;
    protected $guarded = 'admin';
    protected $table = 'tbl_admins';
    protected $fillable = ['name','email','password','role_id','image','phone','status','created_at','updated_at'];
    protected $hidden=['password'];
}
