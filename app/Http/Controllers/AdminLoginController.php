<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{
    //adminLogin
   public function adminLogin(Request $request){
     if($request->isMethod('post')){
              $data = $request->all();
              $rules = [
                'email'=>'required|email|max:255',
                'password'=>'required'
              ];
              $customMessages = [
                'email.required' => "Please Enter Email Address",
                'email.email'=>"Please Enter Valid Emamil Address",
                'email.max'=>"Please Enter less than 255 characters",
                'password.required'=>"Please Enter Password"
              ];
              $this->validate($request,$rules,$customMessages);
           if(Auth::guard('admin')->attempt(['email'=>$data['email'],'password'=>$data['password']])){
             return redirect()->route('adminDashboard');
           }else{
             
             return redirect()->route('adminLogin');
           }
          }
          return view('admin.auth.login');
        }
     
    
      
      
   

   //adminDashboard
   public function dashboard(){
     return view('admin.dashboard');
   }
}
