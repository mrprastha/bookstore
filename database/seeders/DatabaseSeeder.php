<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Admin::insert([
            'name'=>"Prakash",
            'email'=>"prakash@gmail.com",
            'phone'=>"4552154",
            'image'=>'',
            'password'=>bcrypt('password'),
            'role_id'=>1,
            'status'=>1,
            'created_at'=>now(),
            'updated_at'=>now()
        ]);
    }
}
